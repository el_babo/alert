package com.alert.crawl.parse;

import com.alert.singletons.FeedDataService;

public class ParseCrawler {
	public static String removeStopWords(String text)
	{
		text = text.replaceAll("(<.+>|\\-|\\=|\\+|\\*|\\\"|\\?|\\.|\\'|\\,)","").toLowerCase();
		text =  text.replaceAll(FeedDataService.getStopWords(), "");
		return text.replaceAll("\\s+", " ");
	}
}
