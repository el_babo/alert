package com.alert.crawl;

import com.alert.procedures.CrawlSiteProcedure;
import com.alert.singletons.FeedDataService;

public class Crawler {
	public static void main(String[] args) 
	{
		FeedDataService.loadStopWords();
		FeedDataService.loadSites();
		
		CrawlSiteProcedure csp = new CrawlSiteProcedure();
		FeedDataService.getSites().forEachEntry( csp );
				
		/*Site s = new Site("http://feeds.bbci.co.uk/news/world/rss.xml", "BBC");
		Crawler c = new Crawler(s);
		SyndFeed f = c.crawl();

		@SuppressWarnings("unchecked")
		List<SyndEntryImpl> entries = (List<SyndEntryImpl>) f.getEntries();
		
		for(SyndEntryImpl entry : entries)
		{
			System.out.println("---------------------------------------------------------------");
			System.out.println(ParseCrawler.removeStopWords( entry.getDescription().getValue() ));
		}*/
		
	}	
}
