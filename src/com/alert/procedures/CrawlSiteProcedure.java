package com.alert.procedures;

import java.net.URL;
import java.util.ArrayList;

import com.alert.singletons.FeedDataService;
import com.alert.site.Article;
import com.alert.site.Site;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import gnu.trove.procedure.TIntObjectProcedure;

public class CrawlSiteProcedure implements TIntObjectProcedure<Site> {
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean execute(int key, Site s) {
		if( key <= FeedDataService.getSites().size() )
		{
			System.out.println("Crawling: " + s.getSiteName());
			SyndFeed feeds = crawl(s.getUrl()); //connect to the site
			ArrayList<SyndEntryImpl> entries = (ArrayList<SyndEntryImpl>) feeds.getEntries();
			
			for(int i=0;i<entries.size();i++){
				SyndEntryImpl e = (SyndEntryImpl) entries.get(i);
				//article has to be newer than 30mins
				if(e.getPublishedDate().getTime() > (System.currentTimeMillis() - FeedDataService.SECONDS_AGO) )
				{
					Article a = new Article( e );
					s.getArticles().add( a ); //store all the articles in the site object
				}
				//should we compare with other sites?
			}
			return true;
		}else{
			return false;
		}
	}
	
	private SyndFeed crawl(String url)
	{
		try
		{
			URL feedURL = new URL(url);
			
			SyndFeedInput input = new SyndFeedInput();
			return input.build( new XmlReader(feedURL) );
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
