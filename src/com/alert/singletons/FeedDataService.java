package com.alert.singletons;

import gnu.trove.map.hash.TIntObjectHashMap;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.alert.site.*;

public class FeedDataService {
	public static long SECONDS_AGO = 30 * 60 * 1000;
	private static StringBuilder stopWords;
	private static TIntObjectHashMap<Site> sites;
	
	public static String getStopWords()
	{
		return stopWords.toString();
	}
	
	public static TIntObjectHashMap<Site> getSites(){
		return sites;
	}
	public static void loadSites()
	{
		sites = new TIntObjectHashMap<Site>();

		try
		{
			InputStream is = ClassLoader.class.getResourceAsStream("/com/alert/crawl/resources/feeds.txt");
			BufferedReader reader = new BufferedReader( new InputStreamReader( is, "UTF-8") );
			
			String line;
			int counter=0;
			while( (line = reader.readLine()) != null )
			{
				String[] parts = line.split("\\|");
				Site s = new Site(parts[0], parts[1]);
				sites.put(counter,s);
				counter++;
			}
			
			System.out.println("Added " + sites.size() + " sites to memory.");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void loadStopWords()
	{
		stopWords = new StringBuilder();
		stopWords.append("\\b(");
		try
		{
			InputStream is = ClassLoader.class.getResourceAsStream("/com/alert/crawl/resources/ignore_words.txt");
			BufferedReader reader = new BufferedReader( new InputStreamReader( is, "UTF-8") );
			
			String line;
			int counter=0;
			while( (line = reader.readLine()) != null )
			{
				stopWords.append(line + "|");
				counter++;
			}
			stopWords.deleteCharAt( stopWords.lastIndexOf("|") );
			stopWords.append(")\\b");
			System.out.println("Added " + counter + " stop words to memory.");
			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
