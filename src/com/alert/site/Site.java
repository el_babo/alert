package com.alert.site;

import gnu.trove.set.hash.THashSet;

public class Site {
	private String siteName;
	private String url;
	private THashSet<Article> articles;
	
	
	public Site(String siteName, String url) {
		super();
		this.siteName = siteName;
		this.url = url;
		this.articles = new THashSet<Article>();
	}
	public String getSiteName() {
		return siteName;
	}
	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public THashSet<Article> getArticles() {
		return articles;
	}
	public void setArticles(THashSet<Article> articles) {
		this.articles = articles;
	}
	
	
}
