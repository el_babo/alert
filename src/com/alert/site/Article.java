package com.alert.site;

import gnu.trove.set.hash.THashSet;

import java.util.Set;

import com.sun.syndication.feed.synd.SyndEntryImpl;

public class Article {
	SyndEntryImpl entry;
	Set<Article> connections;
	
	
	public Article(SyndEntryImpl entry)
	{
		this.entry = entry;
		connections = new THashSet<Article>();
	}
	
	public SyndEntryImpl getEntry() {
		return entry;
	}
	public void setEntry(SyndEntryImpl entry) {
		this.entry = entry;
	}
	public Set<Article> getConnections() {
		return connections;
	}
	public void setConnections(Set<Article> connections) {
		this.connections = connections;
	}
	
	
}
